Django app template
===================

Codebase initial for use when create an Django app with Docker.

- Settings local
- Compress Minify html, css, js
- Query inspector
- I18N
- Log viewer
- Loggin

Fork from https://github.com/pablotrinidad/cride-platzi/tree/1/Codebase

------------------------------------------------------
(c) Copyright 2021 - Joseph Carhuaz - www.jcarhuazv.com
