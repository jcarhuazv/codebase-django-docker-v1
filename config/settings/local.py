"""Development settings."""

from .base import *  # NOQA
from .base import env, APPS_DIR, STAGE

# Base
DEBUG = env.bool('DJANGO_DEBUG')

# Security
SECRET_KEY = env.str('DJANGO_SECRET_KEY', default='PB3aGvTmCkzaLGRAxDc3aMayKTPTDd5usT8gw4pCmKOk5AlJjh12pTrnNgQyOHCH')
ALLOWED_HOSTS = [
    "localhost",
    "0.0.0.0",
    "127.0.0.1",
]

# Static files
STATIC_ROOT = str(ROOT_DIR('staticfiles'))
STATIC_URL = '/static/'
STATICFILES_DIRS = [
    str(APPS_DIR.path('static')),
]
STATICFILES_FINDERS = [
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    # For use compressor on minifying Django.
    'compressor.finders.CompressorFinder',
]

# Media
MEDIA_ROOT = str(APPS_DIR('media'))
MEDIA_URL = '/media/'

# Cache
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
        'LOCATION': ''
    }
}

# Templates
TEMPLATES[0]['OPTIONS']['debug'] = DEBUG  # NOQA

# Email
EMAIL_BACKEND = env.str('EMAIL_BACKEND', default='django.core.mail.backends.console.EmailBackend')
EMAIL_HOST = env.str('EMAIL_HOST', default='localhost')
EMAIL_PORT = env.int('EMAIL_PORT', default=1025)

# django-extensions
INSTALLED_APPS += ['django_extensions']  # noqa F405

# Middlewares
MIDDLEWARE += [
    # Query Inspector
    'qinspect.middleware.QueryInspectMiddleware',
]

# Loggin settings
LOGGING = {
    'version': 1,
    'disable_existig_loggers': False,
    'formatters': {
        'simple': {
            'format': '{levelname} {asctime} | {message}',
            'style': '{',
        }
    },
    'handlers': {
        'file': {
            'level': 'WARNING',
            'class': 'logging.FileHandler',
            'filename': str(APPS_DIR.path('logs')) + '/debug.log',
            'formatter': 'simple',
        },
        'console': {
            'level': 'INFO',
            'class': 'logging.StreamHandler',
        },
        'console_qinspector': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
        }
    },
    'loggers': {
        'django': {
            'handlers': ['file', 'console'],
            'level': 'INFO',
            'propagate': True,
        },
        'qinspect': {
            'handlers': ['console_qinspector'],
            'level': 'DEBUG',
            'propagate': True,
        },
    },
}


# Amazon S3 settings
if STAGE == 'local':
    AWS_USE_S3 = False
else:
    AWS_USE_S3 = env.bool('AWS_USE_S3')


# Query Inspector
# Whether the Query Inspector should do anything (default: False)
QUERY_INSPECT_ENABLED = True
# Whether to log the stats via Django logging (default: True)
QUERY_INSPECT_LOG_STATS = True
# Whether to add stats headers (default: True)
QUERY_INSPECT_HEADER_STATS = True
# Whether to log duplicate queries (default: False)
QUERY_INSPECT_LOG_QUERIES = True
# Whether to log queries that are above an absolute limit (default: None - disabled)
QUERY_INSPECT_ABSOLUTE_LIMIT = 100  # in milliseconds
# Whether to log queries that are more than X standard deviations
# above the mean query time (default: None - disabled)
QUERY_INSPECT_STANDARD_DEVIATION_LIMIT = 2
# Whether to include tracebacks in the logs (default: False)
QUERY_INSPECT_LOG_TRACEBACKS = False
# Project root (a list of directories, see below - default empty)
QUERY_INSPECT_TRACEBACK_ROOTS = [APPS_DIR]
# Minimum number of duplicates needed to log the query (default: 2)
QUERY_INSPECT_DUPLICATE_MIN = 1  # to force logging of all queries
# Whether to truncate SQL queries in logs to specified size,
# for readability purposes (default: None - full SQL query is included)
QUERY_INSPECT_SQL_LOG_LIMIT = 120  # limit to 120 chars
